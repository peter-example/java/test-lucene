package hk.quantr.test.lucene;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class DeleteIndex {

	@Test
	public void testIndex() {
		try {
			Analyzer analyzer = new StandardAnalyzer();
			IndexWriterConfig indexConfig = new IndexWriterConfig(analyzer);
			File path = new File("index");
			Directory directory = FSDirectory.open(path.toPath());
			IndexWriter indexWriter = new IndexWriter(directory, indexConfig);
			indexWriter.deleteDocuments(new Term("bookId", "book2"));
			indexWriter.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
