package hk.quantr.test.lucene;

import java.io.File;
import java.io.IOException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestSearch {

	@Test
	public void testSearch() {
		try {
			// 1. 创建分析器对象(Analyzer), 用于分词
			Analyzer analyzer = new StandardAnalyzer();

			// 2. 创建查询对象(Query)
			// 2.1 创建查询解析器对象
			// 参数一:默认的搜索域, 参数二:使用的分析器
			QueryParser queryParser = new QueryParser("bookName", analyzer);
			// 2.2 使用查询解析器对象, 实例化Query对象
			Query query = queryParser.parse("bookName:sharepoint");

			// 3. 创建索引库目录位置对象(Directory), 指定索引库位置
			Directory directory = FSDirectory.open(new File("index").toPath());

			// 4. 创建索引读取对象(IndexReader), 用于读取索引
			IndexReader indexReader = DirectoryReader.open(directory);

			// 5. 创建索引搜索对象(IndexSearcher), 用于执行索引
			IndexSearcher searcher = new IndexSearcher(indexReader);

			// 6. 使用IndexSearcher对象执行搜索, 返回搜索结果集TopDocs
			// 参数一:使用的查询对象, 参数二:指定要返回的搜索结果排序后的前n个
			TopDocs topDocs = searcher.search(query, 10);

			// 7. 处理结果集
			// 7.1 打印实际查询到的结果数量
			System.out.println("实际查询到的结果数量: " + topDocs.totalHits);
			System.out.println("-".repeat(100));
			// 7.2 获取搜索的结果数组
			// ScoreDoc中有文档的id及其评分
			ScoreDoc[] scoreDocs = topDocs.scoreDocs;
			for (ScoreDoc scoreDoc : scoreDocs) {
				// 获取文档的id和评分
				int docId = scoreDoc.doc;
				float score = scoreDoc.score;
				System.out.println("文档id= " + docId + " , 评分= " + score);
				// 根据文档Id, 查询文档数据 -- 相当于关系数据库中根据主键Id查询数据
				Document doc = searcher.doc(docId);
				System.out.println("图书Id: " + doc.get("bookId"));
				System.out.println("图书名称: " + doc.get("bookName"));
				System.out.println("图书价格: " + doc.get("bookPrice"));
				System.out.println("图书图片: " + doc.get("bookPic"));
				System.out.println("图书描述: " + doc.get("bookDesc"));
				for (IndexableField i : doc.getFields("author")) {
					System.out.println("Author: " + i.stringValue());
				}
				System.out.println("-".repeat(100));
			}

			// 8. 关闭资源
			indexReader.close();
		} catch (IOException | ParseException ex) {
			ex.printStackTrace();
		}
	}
}
