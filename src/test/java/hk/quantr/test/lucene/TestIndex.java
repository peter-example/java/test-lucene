package hk.quantr.test.lucene;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FloatDocValuesField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestIndex {

	@Test
	public void testIndex() {
		try {
			List<Document> documents = new ArrayList<>();
			Document document = new Document();
			document.add(new TextField("bookId", "book1", Field.Store.YES));
			document.add(new TextField("bookName", "Masting java in 12 days", Field.Store.YES));
			document.add(new TextField("bookId", "book1", Field.Store.YES));
			document.add(new TextField("author", "peter", Field.Store.YES));
			document.add(new TextField("author", "leo", Field.Store.YES));
			document.add(new FloatDocValuesField("bookPrice", 12.34f));
			documents.add(document);

			document = new Document();
			document.add(new TextField("bookId", "book2", Field.Store.YES));
			document.add(new TextField("bookName", "SharePoint book", Field.Store.YES));
			document.add(new TextField("author", "ken", Field.Store.YES));
			document.add(new TextField("author", "Mary", Field.Store.YES));
			document.add(new FloatDocValuesField("bookPrice", 34.56f));
			documents.add(document);

			// 3. 创建分析器对象(Analyzer), 用于分词
			Analyzer analyzer = new StandardAnalyzer();
			// 4. 创建索引配置对象(IndexWriterConfig), 用于配置Lucene
			// 参数一:当前使用的Lucene版本, 参数二:分析器
			IndexWriterConfig indexConfig = new IndexWriterConfig(analyzer);
			// 5. 创建索引库目录位置对象(Directory), 指定索引库的存储位置
			File path = new File("index");
			Directory directory = FSDirectory.open(path.toPath());
			// 6. 创建索引写入对象(IndexWriter), 将文档对象写入索引
			IndexWriter indexWriter = new IndexWriter(directory, indexConfig);
			// 7. 使用IndexWriter对象创建索引
			for (Document doc : documents) {
				// addDocement(doc): 将文档对象写入索引库
				indexWriter.addDocument(doc);
			}
			// 8. 释放资源
			indexWriter.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
